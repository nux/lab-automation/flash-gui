
from PyQt5.QtWidgets import (QApplication)
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QFont 

from PyQt5.QtWidgets import (QComboBox, QMainWindow, QVBoxLayout, QHBoxLayout,
                             QApplication, QWidget, QLabel, QLineEdit,
                             QPushButton, QFrame,QGroupBox,QPlainTextEdit,QTextEdit, QSizePolicy,QGridLayout,QCheckBox,QSlider, QTreeWidget, QTreeWidgetItem, QTableWidget, QTableWidgetItem,QMessageBox
                             )

import sys
import time
import yaml
import json
import datetime
import threading

from fablive.daqmanager import DAQManager
from fablive import Run


def convert_type(val, typestring):
    
    if typestring=='float':
        try:
            return float(val)
        except:
            return None

    if typestring=='int':
        try:
            return int(val)
        except:
            return None
    
    if typestring=='string' or typestring=='list':
        try:
            return str(val)
        except:
            return None

    else:
        return None
    
def check_range(val, ran, islist):
    if ran is not None:
        if islist:
            return val in ran
        else:
            return (val < ran[1]) and (val > ran[0])
    else:
        return 1

class Entry(QGroupBox):
    def __init__(self, entry, parent=None):
        super(QGroupBox, self).__init__()
        self.entry = entry
        layout = QGridLayout() 


        #if self.entry['type'] != 'list'
        self.valtext = QLineEdit()
        self.valtext.setText(str(self.entry["default"]))
        self.valtext.setFixedWidth(int(80*sizescale))
        self.valtext.setAlignment(Qt.AlignRight)

        


        self.messlabel = QLabel()
        self.messlabel.setFixedWidth(int(120*sizescale))
        self.messlabel.setAlignment(Qt.AlignCenter)

        #self.messlabel.setFixedSize()
        
        self.checkbutt = QCheckBox("Confirm")
        self.checkbutt.setChecked(entry['check'])
        #self.checkbutt.clicked.connect(self.readvalue) 

        if self.entry["type"] != 'list':
            if self.entry["range"] is not None:
                rangestring = "( " + str(self.entry["range"][0]) + " , " + str(self.entry["range"][1]) + " )"
            else:
                rangestring = ""
            self.rangelabel = QLabel(rangestring)
            self.rangelabel.setAlignment(Qt.AlignCenter)
            
        else:
            self.rangelabel = QComboBox()
            self.rangelabel.setEnabled(True)
            for val in self.entry['range']:
                self.rangelabel.addItem(val) 
            self.rangelabel.setCurrentText(self.entry['range'][0])
            self.rangelabel.currentTextChanged.connect(self.setfromlist)
            

        self.rangelabel.setFixedWidth(int(80*sizescale))
        layout.addWidget(self.rangelabel,0,3,1,1)        

        desclabel = QLabel(entry["desc"])
        desclabel.setFixedWidth(int(260*sizescale))
        desclabel.setAlignment(Qt.AlignRight)

        if entry['unit'] is not None:
            unitlabel = QLabel(entry["unit"])
        else:
            unitlabel = QLabel("")
            
        unitlabel.setFixedWidth(int(35*sizescale))
        
        layout.addWidget(desclabel,0,0,1,1)        
        layout.addWidget(self.valtext,0,1,1,1)
        layout.addWidget(unitlabel,0,2,1,1)        
        layout.addWidget(self.messlabel,0,4,1,1)        
        layout.addWidget(self.checkbutt,0,5,1,1)

#        layout.setSpacing(0)        
        layout.setContentsMargins(0,0,0,0)        
        self.setLayout(layout)
    
    def msg(self, mtype, mtext):
        if mtype=="o":
            self.messlabel.setText(mtext)
            self.messlabel.setStyleSheet("background-color: lightgreen")

        if mtype=="w":
            self.messlabel.setText(mtext)
            self.messlabel.setStyleSheet("background-color: yellow")

        if mtype=="e":
            self.messlabel.setText(mtext)
            self.messlabel.setStyleSheet("background-color: red")
    
    def setfromlist(self):
        strval = str(self.rangelabel.currentText())
        self.valtext.setText(strval)

    def confirmvalue(self):
        
        if self.checkbutt.isChecked():
            unstring = ''
            if self.entry['unit'] is not None:
                unstring=self.entry['unit'] 
            reply = QMessageBox.question(self, 'Value Check', 'Is ' + self.entry['desc'] + ' = ' + str(self.readvalue()) + " " + unstring+ ' ?', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)     
            
            if reply == QMessageBox.No:
                return False
            else:
                return True
        else:
            return True
    
    def readvalue(self):
        
        strval = str(self.valtext.text())
        val = convert_type(strval, self.entry["type"])
        if val is None:
            self.msg("e", "Wrong format")
            return None
        
        rangeisvalid = check_range(val, self.entry["range"],self.entry["type"]=='list' )
        if rangeisvalid==False:
            self.msg("w", "Out of range")
            return None
        
        else:
            self.msg("o", "OK")
            return val
            
    def setvalue(self, val):
        
        strval = str(val)
        self.valtext.setText(strval)
        

        
        
        
        


class EntryList(QGroupBox):
    def __init__(self, entries, parent=None):
        super(QGroupBox, self).__init__()
        self.entries = entries
        
        self.entryWidgets = []
        for entry in self.entries:
            ew = Entry(entry, self)
            self.entryWidgets.append(ew)
        
        
        layout = QGridLayout() 

        for i, ew in enumerate(self.entryWidgets):
            layout.addWidget(ew, i,0,1,1)

        layout.setSpacing(0)        
        layout.setContentsMargins(0,0,0,0)        
        self.setLayout(layout)
        
    def readvalues(self):
        
        paramvals = {}
        
        for ie, entry in enumerate(self.entries):     
            val = self.entryWidgets[ie].readvalue()
            
            paramvals[entry["name"]] = val
            
        
        for p in paramvals.keys():
            if paramvals[p] is None:
                return None
        
        return paramvals
    
    def setvalues(self, newvalues):
        for ie, entry in enumerate(self.entries):
            if entry['name'] in newvalues.keys():
                self.entryWidgets[ie].setvalue(newvalues[entry['name']])

    def confirmvalues(self):
        for ie, entry in enumerate(self.entries):     
            val = self.entryWidgets[ie].confirmvalue()      
            if not val:
                return False
        return True

class ControlsLayout(QGroupBox):
    acqstarted = pyqtSignal()
    acqstopped = pyqtSignal()
    acqerror = pyqtSignal()
    
    def __init__(self, parent=None):
        super(QGroupBox, self).__init__()

        self.daqmanager = DAQManager(mock=TEST)
        self.daqrunning = -1
        
        self.parent = parent
        layout = QGridLayout() 

        self.acquirebutt = QPushButton('Start', self)
        self.acquirebutt.clicked.connect(self.startacq) 


        self.statuslabel = QLabel()
        self.statuslabel.setAlignment(Qt.AlignCenter)
        #self.statuslabel.setFixedWidth(int(80*sizescale))

        self.stopbutt = QPushButton('Stop', self)
        self.stopbutt.clicked.connect(self.stopacq) 
        
        #self.stopstatus()
        


        layout.addWidget(self.acquirebutt,0,0,1,1)        
        layout.addWidget(self.stopbutt,1,0,1,1)
        layout.addWidget(self.statuslabel,2,0,1,2)        


        self.acquirebutt.setFixedWidth(int(160*sizescale))
        self.stopbutt.setFixedWidth(int(160*sizescale))
        self.statuslabel.setFixedWidth(int(160*sizescale))
        self.acquirebutt.setFixedHeight(int(40*sizescale))
        self.stopbutt.setFixedHeight(int(40*sizescale))

        
        self.acqstarted.connect(self.drawdaqrunning)
        self.acqstopped.connect(self.drawdaqstopped)
        self.acqerror.connect(self.drawdaqerror)
        
        
        self.setLayout(layout)
        
        
        self.statusthread = threading.Thread(target=self.checkdaqstatus)
        self.statusthread.start()

    #def stopstatus(self):

        #self.statuslabel.setText("Not running")
        #self.statuslabel.setStyleSheet("background-color: lightyellow")
        #self.statuslabel.repaint()

    #def runstatus(self):

        #self.statuslabel.setText("Running")
        #self.statuslabel.setStyleSheet("background-color: lightgreen")    
        #self.statuslabel.repaint()
    
    @pyqtSlot()
    def drawdaqrunning(self):
        runno = self.daqmanager.run_number
        self.statuslabel.setText("Acquiring\nRun "+str(runno))
        self.statuslabel.setStyleSheet("background-color: lightgreen") 
        self.statuslabel.repaint()

    @pyqtSlot()
    def drawdaqstopped(self):
        runno = self.daqmanager.run_number
        self.statuslabel.setText("Idle\nLast Run: "+str(runno))
        self.statuslabel.setStyleSheet("background-color: lightyellow")  
        self.statuslabel.repaint()
    
    @pyqtSlot()
    def drawdaqerror(self):
        #runno = self.daqmanager.run_number
        self.statuslabel.setText("Error")
        self.statuslabel.setStyleSheet("background-color: red") 
        self.statuslabel.repaint()

        
    def checkdaqstatus(self):
        while 1:
            oldstat = self.daqrunning
            self.daqrunning = self.daqmanager.status
            runno = self.daqmanager.run_number
            if self.daqrunning==1 and oldstat != self.daqrunning:
                self.acqstarted.emit()
            if self.daqrunning==0 and oldstat != self.daqrunning:
                self.acqstopped.emit()            
            
            if self.daqrunning!=0 and self.daqrunning!=1 and  oldstat != self.daqrunning:
                self.acqerror.emit()
            
            time.sleep(0.3)

    def startacq(self):
        self.parent.savevalues()
        
        values = self.parent.readvalues()
        if values is not None:
            checkval = self.parent.confirmvalues()
            
            if checkval:
                #self.checkdaqstatus()
                if self.daqrunning == 0:
                    self.started = True
                    with Run(daq=self.daqmanager, **values):
                        while self.started == True:
                            time.sleep(0.1)
                            #self.checkdaqstatus()
                            QApplication.processEvents()
                return
        
        #self.stopstatus()
        return
    
    
    def stopacq(self):
        self.started = False
        return None


class ValuesLayout(QGroupBox):
    def __init__(self, parent=None):
        super(QGroupBox, self).__init__()
        self.parent = parent
        layout = QGridLayout() 

        

        self.savebutton = QPushButton('Save', self)
        self.savebutton.clicked.connect(self.savevalues) 


        self.loadbutton = QPushButton('Load', self)
        self.loadbutton.clicked.connect(self.loadvalues) 
        
        title = QLabel("Configuration")
        title.setAlignment(Qt.AlignCenter)
        
        layout.addWidget(title,0,0,1,2)        
        layout.addWidget(self.savebutton,1,0,1,1)        
        layout.addWidget(self.loadbutton,1,1,1,1)

        self.loadbutton.setFixedWidth(int(80*sizescale))
        self.savebutton.setFixedWidth(int(80*sizescale))
   

        self.setLayout(layout)

    def savevalues(self):
        self.parent.savevalues()
        
    def loadvalues(self):
        self.parent.loadvalues()

        
class MainWindow(QMainWindow):
    def __init__(self, conffile, logfile, parent=None, width=16, height=9, dpi=160):
        super(MainWindow, self).__init__()
        self.width = width
        self.height = height
        self.dpi = dpi
        self.conffile=conffile
        self.logfile = logfile
        self.loadconf()
        self.setup()

    def setup(self):
        #if not TEST:
            
        #self.fig = Figure(figsize=(self.width, self.height), dpi=self.dpi)
        
        
        ################################### Load runs layout ####
        
        self.EntryGroup = EntryList(self.entries, self)
        self.ControlsGroup = ControlsLayout(self)
        self.ValuesGroup = ValuesLayout(self)

        
        ####################################
        
        
        mainLayout = QGridLayout()
        
        mainLayout.addWidget(self.EntryGroup, 0,0,2,1)
        mainLayout.addWidget(self.ControlsGroup, 0,1,1,1)
        mainLayout.addWidget(self.ValuesGroup, 1,1,1,1)


        mainLayout.setRowStretch(0, 4)
        mainLayout.setRowStretch(1, 1)

        mainWidget = QWidget(self)
        mainWidget.setLayout(mainLayout)
        
        self.setCentralWidget(mainWidget)

        self.readvalues()
        self.show()
        
    def readvalues(self):
        return self.EntryGroup.readvalues()
    def confirmvalues(self):
        return self.EntryGroup.confirmvalues()
    
    def savevalues(self):
        vals = self.readvalues()
        if vals is not None:
            outdict = {
                "timestamp": str(datetime.datetime.now()),
                "values" : vals
                }
            outjson = json.dumps(outdict)
            print(outjson)
            
            with open(self.logfile, mode='a') as f:
                f.write(outjson+"\n")
            
    def loadvalues(self):
        with open(self.logfile, 'r') as f:
            lastlog = f.readlines()[-1]
            lastlog = json.loads(lastlog)
            
        lastvalues = lastlog["values"]
        self.EntryGroup.setvalues(lastvalues)
        self.readvalues()
        
        
    def loadconf(self):
        
        with open(self.conffile, "r") as file:
        
            confdict = yaml.load(file, Loader=yaml.Loader)
            print(confdict)
            self.entries = []
            
            for k in confdict.keys():
                confdict[k]["name"] = k
                self.entries.append(confdict[k])
                    
            return 



#################d

TEST=False

sizescale = 1.5


app = QApplication(sys.argv)
app.setFont(QFont("Roboto", 15))
app.setAttribute(Qt.AA_EnableHighDpiScaling, True)
w = MainWindow(conffile = "./DAQ_conf.yml", logfile = "./DAQ_log.json")
app.exec_()



